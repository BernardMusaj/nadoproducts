﻿using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.DataAccess.Konkret.EntityFramework.Contexts;
using NadoProducts.Entitete.Konkret;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NadoProducts.DataAccess.Konkret.EntityFramework
{
    public class EfKategoriDal : EfEntityRepositoryBase<Kategori, NadoProductsContext>, IKategoryDAL
    {
    }
}
