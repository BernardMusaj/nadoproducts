﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.DataAccess.Konkret.EntityFramework.Mappings
{
    public class PerdoruesMap:EntityTypeConfiguration<Perdorues>
    {
        public PerdoruesMap()
        {
            HasKey(t => t.Id);
            Property(t => t.PerdoruesEmri).IsRequired();
            Property(t => t.Pass).IsRequired();

            ToTable("Perdoruesit");

            Property(t => t.Id).HasColumnName("Id");
            Property(t => t.PerdoruesEmri).HasColumnName("PerdoruesEmri");
            Property(t => t.Pass).HasColumnName("Pass");
        }
    }
}
