﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.DataAccess.Konkret.EntityFramework.Mappings
{
    public class ProduktMap:EntityTypeConfiguration<Produkt>
    {
        public ProduktMap()
        {
            HasKey(t => t.Id);

            Property(t => t.ProduktEmer).IsRequired().HasMaxLength(40);
            Property(t => t.SasiPerCope).HasMaxLength(20);

            ToTable("Produktet");
                                            //Mapping
            Property(t => t.Id).HasColumnName("ProduktId");
            Property(t => t.ProduktEmer).HasColumnName("ProduktEmer");
            Property(t => t.KategoriId).HasColumnName("KategoriId");
            Property(t => t.SasiPerCope).HasColumnName("SasiPerCope");
            Property(t => t.CmimiCope).HasColumnName("CmimiCope");
            Property(t => t.CopeNeStok).HasColumnName("CopeNeStok");

            //Per te krijuar relacionin e "Kategori" tek "Produkt"
            HasOptional(t => t.Kategori).WithMany(t => t.Produktet).HasForeignKey(d => d.KategoriId);

        }
    }
}
