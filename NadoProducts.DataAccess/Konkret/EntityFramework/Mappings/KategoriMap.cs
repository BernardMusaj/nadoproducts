﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.DataAccess.Konkret.EntityFramework.Mappings
{
    public class KategoriMap:EntityTypeConfiguration<Kategori>
    {
        public KategoriMap()
        {
            HasKey(t => t.Id);
            Property(t => t.KategoriEmer).IsRequired().HasMaxLength(15);

            ToTable("Kategorite");

            Property(t => t.Id).HasColumnName("KategoriId");
            Property(t => t.KategoriEmer).HasColumnName("KategoriEmer");
            Property(t => t.Pershkrim).HasColumnName("KategoriPershkrim");
        }
    }
}
