﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.Entitete.Abstrakt;
using System.Data.Entity;
using System.Linq.Expressions;

namespace NadoProducts.DataAccess.Konkret.EntityFramework
{
    public class EfEntityRepositoryBase<TEntity, TContex> : IEntitetRepository<TEntity>
        where TEntity : class, IEntitet, new()
        where TContex : DbContext, new()
    {
        public void Add(TEntity entity)
        {
            using (var context = new TContex())
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            using (var context = new TContex())
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            using (var context = new TContex())
            {
                return context.Set<TEntity>().SingleOrDefault(filter);
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null, 
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            int? page = default(int?),
            int? pageSize = default(int?))
        {
            using (var context = new TContex())
            {
                IQueryable<TEntity> query = context.Set<TEntity>();

                if (filter != null)
                {
                    query = query.Where(filter);
                }

                if (orderBy != null)
                {
                    query = orderBy(query);
                }

                //psh nqs       "pageSize" = 10
                //dhe jemi tek  "page" = 2
                //              "Numri Artikujve" = 100
                if(page != null && pageSize != null)
                {
                    query = query.Skip((page.Value - 1)*pageSize.Value).Take(pageSize.Value);
                }

                return query.ToList();
            }
        }

        public void Update(TEntity entity)
        {
            using (var context = new TContex())
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
