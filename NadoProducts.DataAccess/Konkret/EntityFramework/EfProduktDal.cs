﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;
using NadoProducts.DataAccess.Konkret.EntityFramework.Contexts;
using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.Entitete.Tipet_Komplekse;

namespace NadoProducts.DataAccess.Konkret.EntityFramework
{
    public class EfProduktDal : EfEntityRepositoryBase<Produkt, NadoProductsContext>, IProduktDAL
    {
        public List<ProduktDetaj> GetProduktDetaj()
        {
            throw new NotImplementedException();
        }

        public int GetProduktetCountByKategori(int? kategoriId)
        {
            using (var context = new NadoProductsContext())
            {
                if (kategoriId == null)
                {
                    return context.Produktet.Count();
                }

                return context.Produktet.Count(p => p.KategoriId == kategoriId);
            }
        }

        
    }
}
