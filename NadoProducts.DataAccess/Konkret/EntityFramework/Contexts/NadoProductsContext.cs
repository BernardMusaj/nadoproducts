﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using NadoProducts.Entitete.Konkret;
using NadoProducts.DataAccess.Konkret.EntityFramework.Mappings;

namespace NadoProducts.DataAccess.Konkret.EntityFramework.Contexts
{
    public class NadoProductsContext:DbContext
    {
        public NadoProductsContext():base("Name=NadoProductsContext") //Konstruktori i kases base me parameter CS
        {

        }

        public DbSet<Kategori> Kategorite { get; set; }
        public DbSet<Produkt> Produktet { get; set; }
        public DbSet<Perdorues> Perdoruesit { get; set; }

        //Map modelet e mia nga Maps qe kam krijuar 

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PerdoruesMap());
            modelBuilder.Configurations.Add(new ProduktMap());
            modelBuilder.Configurations.Add(new KategoriMap());
        }
    }
}
