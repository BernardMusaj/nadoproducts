﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;
using NadoProducts.Entitete.Tipet_Komplekse;


namespace NadoProducts.DataAccess.Abstrakt
{
    public interface IProduktDAL:IEntitetRepository<Produkt>
    {
        List<ProduktDetaj> GetProduktDetaj();

        int GetProduktetCountByKategori(int? kategoriId);
    }
}
