﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Abstrakt;
using System.Linq.Expressions;

namespace NadoProducts.DataAccess.Abstrakt
{                                      //ku "T" do jete nje klase qe do implementohet nga "IEntitet"
    public interface IEntitetRepository<T> where T:class, IEntitet, new()
    {
        T Get(Expression<Func<T,bool>> filter); //Mer vetem nje Entitet bazuar ne shprehjen e cila default esht null

        //Nqs nuk jepen parametra, ath do te kthehet e gjithe lista //Mer nje list me Entitete bazuar ne shprehjen
        //(default esht null, qe do te thote, perdoruesi mund te mos jape parametra)

        List<T> GetList(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>,IOrderedQueryable<T>> orderBy = null,
            int? page = null,
            int? pageSize = null);

        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
