﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.DataAccess.Abstrakt
{
    public interface IKategoryDAL:IEntitetRepository<Kategori>
    {
    }
}
