﻿using System.Collections.Generic;
using NadoProducts.Entitete.Abstrakt;

namespace NadoProducts.Entitete.Konkret
{
    public class Kategori:IEntitet
    {
        public Kategori()                        //Konstruktori inicion listen e produkteve
        {
            Produktet = new List<Produkt>();
        }

        public int Id { get; set; }
        public string KategoriEmer { get; set; }
        public string Pershkrim { get; set; }
        public List<Produkt> Produktet { get; set; } //Lista produkteve qe perfshihen ne kete kategori
    }
}