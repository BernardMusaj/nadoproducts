﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Abstrakt;

namespace NadoProducts.Entitete.Konkret
{
    public class Perdorues:IEntitet
    {
        public int Id { get; set; }
        public string PerdoruesEmri { get; set; }
        public string Pass { get; set; }
    }
}
