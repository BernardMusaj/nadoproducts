﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Abstrakt; //Qe te implementojm Entitete e DB nga IEntitet

namespace NadoProducts.Entitete.Konkret
{
    public class Produkt:IEntitet
    {
        public int Id { get; set; }
        public string ProduktEmer { get; set; }
        public int? KategoriId { get; set; }      //Eshte cilesi Nullable duke i vendosur "?" 
        public string SasiPerCope { get; set; }
        public decimal? CmimiCope { get; set; }
        public short? CopeNeStok { get; set; }
        public Kategori Kategori { get; set; } //Cdo produkt ka vetem nje kategori
    }
}
