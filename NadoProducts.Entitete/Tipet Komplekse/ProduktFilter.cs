﻿namespace NadoProducts.Entitete
{
    public class ProduktFilter
    {
        public int? KategoriId { get; set; }
        public int? Page { get; set; }
        public int? PageSize{ get; set; }
        public int? ProduktEmri { get; set; }
    }
}