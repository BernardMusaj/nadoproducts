﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NadoProducts.Entitete.Tipet_Komplekse
{
    public class ProduktDetaj                       //Nuk eshte table DB-je, eshte tip kompleks 
                                           //(Mund te perdoret ne tgjitha operacionet por jo si entitet DB-je)
    {                                               //E panevojshme te implemnetohet nga "IEntitet"
        public int ProduktId { get; set; }
        public string ProduktEmer { get; set; }
        public string KategoriEmer { get; set; }
        public string SasiPerCope { get; set; }
        public decimal CmimiCope { get; set; }
        public short? CopeNeStok { get; set; }
    }
}
