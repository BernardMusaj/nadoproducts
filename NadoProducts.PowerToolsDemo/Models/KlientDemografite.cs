using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class KlientDemografite
    {
        public KlientDemografite()
        {
            this.Klientets = new List<Klientet>();
        }

        public string KlientTipId { get; set; }
        public string KlientPershkrim { get; set; }
        public virtual ICollection<Klientet> Klientets { get; set; }
    }
}
