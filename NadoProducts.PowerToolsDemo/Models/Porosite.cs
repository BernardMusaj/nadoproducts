using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Porosite
    {
        public Porosite()
        {
            this.PorosiDetajes = new List<PorosiDetaje>();
        }

        public int PorosiId { get; set; }
        public string KlientId { get; set; }
        public Nullable<int> PuntorId { get; set; }
        public Nullable<System.DateTime> DataPorosi { get; set; }
        public Nullable<System.DateTime> DataNevojshme { get; set; }
        public Nullable<System.DateTime> DataDerguar { get; set; }
        public Nullable<int> DerguarMe { get; set; }
        public Nullable<decimal> Ngarkesa { get; set; }
        public string DergeseEmere { get; set; }
        public string DergeseAdres { get; set; }
        public string DergeseQytet { get; set; }
        public string DergeseRajon { get; set; }
        public string DergeseKodiPostar { get; set; }
        public string DergeseShtet { get; set; }
        public virtual Klientet Klientet { get; set; }
        public virtual ICollection<PorosiDetaje> PorosiDetajes { get; set; }
        public virtual Puntoret Puntoret { get; set; }
        public virtual Transportuesit Transportuesit { get; set; }
    }
}
