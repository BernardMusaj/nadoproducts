using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Rajon
    {
        public Rajon()
        {
            this.Territorets = new List<Territoret>();
        }

        public int RajonId { get; set; }
        public string RajonPershkrim { get; set; }
        public virtual ICollection<Territoret> Territorets { get; set; }
    }
}
