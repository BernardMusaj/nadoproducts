using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Klientet
    {
        public Klientet()
        {
            this.Porosites = new List<Porosite>();
            this.KlientDemografites = new List<KlientDemografite>();
        }

        public string KlientId { get; set; }
        public string EmriKompanise { get; set; }
        public string EmriKontaktit { get; set; }
        public string TitulliKontaktit { get; set; }
        public string Adresa { get; set; }
        public string Qyteti { get; set; }
        public string Rajoni { get; set; }
        public string KodiPostar { get; set; }
        public string Shteti { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public virtual ICollection<Porosite> Porosites { get; set; }
        public virtual ICollection<KlientDemografite> KlientDemografites { get; set; }
    }
}
