using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Produktet
    {
        public Produktet()
        {
            this.PorosiDetajes = new List<PorosiDetaje>();
        }

        public int ProduktId { get; set; }
        public string ProduktEmer { get; set; }
        public Nullable<int> FurnizuesId { get; set; }
        public Nullable<int> KategoriId { get; set; }
        public string SasiPerCope { get; set; }
        public Nullable<decimal> CmimiCope { get; set; }
        public Nullable<short> CopeNeStok { get; set; }
        public Nullable<short> CopeNePorosi { get; set; }
        public Nullable<short> NivelPerseritjesPorosi { get; set; }
        public bool Nderprere { get; set; }
        public virtual Furnizuesit Furnizuesit { get; set; }
        public virtual Kategorite Kategorite { get; set; }
        public virtual ICollection<PorosiDetaje> PorosiDetajes { get; set; }
    }
}
