using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Puntoret
    {
        public Puntoret()
        {
            this.Porosites = new List<Porosite>();
            this.Puntoret1 = new List<Puntoret>();
            this.Territorets = new List<Territoret>();
        }

        public int PuntorId { get; set; }
        public string Mbiemri { get; set; }
        public string Emri { get; set; }
        public string Titulli { get; set; }
        public string TitullMirsjellje { get; set; }
        public Nullable<System.DateTime> Datelindja { get; set; }
        public Nullable<System.DateTime> DataPunesimit { get; set; }
        public string Adresa { get; set; }
        public string Qyteti { get; set; }
        public string Rajoni { get; set; }
        public string KodiPostar { get; set; }
        public string Shteti { get; set; }
        public string TelShtepi { get; set; }
        public string Zgjatimi { get; set; }
        public byte[] Foto { get; set; }
        public string Shenime { get; set; }
        public Nullable<int> RaporteNdaj { get; set; }
        public string FotoPath { get; set; }
        public virtual ICollection<Porosite> Porosites { get; set; }
        public virtual ICollection<Puntoret> Puntoret1 { get; set; }
        public virtual Puntoret Puntoret2 { get; set; }
        public virtual ICollection<Territoret> Territorets { get; set; }
    }
}
