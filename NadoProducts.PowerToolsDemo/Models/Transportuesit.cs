using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Transportuesit
    {
        public Transportuesit()
        {
            this.Porosites = new List<Porosite>();
        }

        public int TransportuesId { get; set; }
        public string EmriKompanise { get; set; }
        public string Tel { get; set; }
        public virtual ICollection<Porosite> Porosites { get; set; }
    }
}
