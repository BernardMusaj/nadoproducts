using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Perdoruesit
    {
        public int Id { get; set; }
        public string PerdoruesEmri { get; set; }
        public string Pass { get; set; }
    }
}
