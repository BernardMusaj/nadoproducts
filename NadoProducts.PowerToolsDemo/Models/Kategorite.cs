using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Kategorite
    {
        public Kategorite()
        {
            this.Produktets = new List<Produktet>();
        }

        public int KategoriId { get; set; }
        public string KategoriEmer { get; set; }
        public string KategoriPershkrim { get; set; }
        public byte[] KategoriFoto { get; set; }
        public virtual ICollection<Produktet> Produktets { get; set; }
    }
}
