using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class TerritoretMap : EntityTypeConfiguration<Territoret>
    {
        public TerritoretMap()
        {
            // Primary Key
            this.HasKey(t => t.TerritorId);

            // Properties
            this.Property(t => t.TerritorId)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.TerritorPershkrim)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Territoret");
            this.Property(t => t.TerritorId).HasColumnName("TerritorId");
            this.Property(t => t.TerritorPershkrim).HasColumnName("TerritorPershkrim");
            this.Property(t => t.RajonId).HasColumnName("RajonId");

            // Relationships
            this.HasRequired(t => t.Rajon)
                .WithMany(t => t.Territorets)
                .HasForeignKey(d => d.RajonId);

        }
    }
}
