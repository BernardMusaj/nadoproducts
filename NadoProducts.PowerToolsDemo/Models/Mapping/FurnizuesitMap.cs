using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class FurnizuesitMap : EntityTypeConfiguration<Furnizuesit>
    {
        public FurnizuesitMap()
        {
            // Primary Key
            this.HasKey(t => t.FurnizuesId);

            // Properties
            this.Property(t => t.EmriKompanise)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.EmriKontaktit)
                .HasMaxLength(30);

            this.Property(t => t.TitulliKontaktit)
                .HasMaxLength(30);

            this.Property(t => t.Adresa)
                .HasMaxLength(60);

            this.Property(t => t.Qyteti)
                .HasMaxLength(15);

            this.Property(t => t.Rajoni)
                .HasMaxLength(15);

            this.Property(t => t.KodiPostar)
                .HasMaxLength(10);

            this.Property(t => t.Shteti)
                .HasMaxLength(15);

            this.Property(t => t.Tel)
                .HasMaxLength(24);

            this.Property(t => t.Fax)
                .HasMaxLength(24);

            // Table & Column Mappings
            this.ToTable("Furnizuesit");
            this.Property(t => t.FurnizuesId).HasColumnName("FurnizuesId");
            this.Property(t => t.EmriKompanise).HasColumnName("EmriKompanise");
            this.Property(t => t.EmriKontaktit).HasColumnName("EmriKontaktit");
            this.Property(t => t.TitulliKontaktit).HasColumnName("TitulliKontaktit");
            this.Property(t => t.Adresa).HasColumnName("Adresa");
            this.Property(t => t.Qyteti).HasColumnName("Qyteti");
            this.Property(t => t.Rajoni).HasColumnName("Rajoni");
            this.Property(t => t.KodiPostar).HasColumnName("KodiPostar");
            this.Property(t => t.Shteti).HasColumnName("Shteti");
            this.Property(t => t.Tel).HasColumnName("Tel");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.HomePage).HasColumnName("HomePage");
        }
    }
}
