using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class PuntoretMap : EntityTypeConfiguration<Puntoret>
    {
        public PuntoretMap()
        {
            // Primary Key
            this.HasKey(t => t.PuntorId);

            // Properties
            this.Property(t => t.Mbiemri)
                .IsRequired()
                .HasMaxLength(20);

            this.Property(t => t.Emri)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.Titulli)
                .HasMaxLength(30);

            this.Property(t => t.TitullMirsjellje)
                .HasMaxLength(25);

            this.Property(t => t.Adresa)
                .HasMaxLength(60);

            this.Property(t => t.Qyteti)
                .HasMaxLength(15);

            this.Property(t => t.Rajoni)
                .HasMaxLength(15);

            this.Property(t => t.KodiPostar)
                .HasMaxLength(10);

            this.Property(t => t.Shteti)
                .HasMaxLength(15);

            this.Property(t => t.TelShtepi)
                .HasMaxLength(24);

            this.Property(t => t.Zgjatimi)
                .HasMaxLength(4);

            this.Property(t => t.FotoPath)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("Puntoret");
            this.Property(t => t.PuntorId).HasColumnName("PuntorId");
            this.Property(t => t.Mbiemri).HasColumnName("Mbiemri");
            this.Property(t => t.Emri).HasColumnName("Emri");
            this.Property(t => t.Titulli).HasColumnName("Titulli");
            this.Property(t => t.TitullMirsjellje).HasColumnName("TitullMirsjellje");
            this.Property(t => t.Datelindja).HasColumnName("Datelindja");
            this.Property(t => t.DataPunesimit).HasColumnName("DataPunesimit");
            this.Property(t => t.Adresa).HasColumnName("Adresa");
            this.Property(t => t.Qyteti).HasColumnName("Qyteti");
            this.Property(t => t.Rajoni).HasColumnName("Rajoni");
            this.Property(t => t.KodiPostar).HasColumnName("KodiPostar");
            this.Property(t => t.Shteti).HasColumnName("Shteti");
            this.Property(t => t.TelShtepi).HasColumnName("TelShtepi");
            this.Property(t => t.Zgjatimi).HasColumnName("Zgjatimi");
            this.Property(t => t.Foto).HasColumnName("Foto");
            this.Property(t => t.Shenime).HasColumnName("Shenime");
            this.Property(t => t.RaporteNdaj).HasColumnName("RaporteNdaj");
            this.Property(t => t.FotoPath).HasColumnName("FotoPath");

            // Relationships
            this.HasMany(t => t.Territorets)
                .WithMany(t => t.Puntorets)
                .Map(m =>
                    {
                        m.ToTable("TerritoretPunonjes");
                        m.MapLeftKey("PuntorId");
                        m.MapRightKey("TerritorId");
                    });

            this.HasOptional(t => t.Puntoret2)
                .WithMany(t => t.Puntoret1)
                .HasForeignKey(d => d.RaporteNdaj);

        }
    }
}
