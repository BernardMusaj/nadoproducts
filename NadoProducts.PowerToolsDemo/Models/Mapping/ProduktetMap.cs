using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class ProduktetMap : EntityTypeConfiguration<Produktet>
    {
        public ProduktetMap()
        {
            // Primary Key
            this.HasKey(t => t.ProduktId);

            // Properties
            this.Property(t => t.ProduktEmer)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.SasiPerCope)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Produktet");
            this.Property(t => t.ProduktId).HasColumnName("ProduktId");
            this.Property(t => t.ProduktEmer).HasColumnName("ProduktEmer");
            this.Property(t => t.FurnizuesId).HasColumnName("FurnizuesId");
            this.Property(t => t.KategoriId).HasColumnName("KategoriId");
            this.Property(t => t.SasiPerCope).HasColumnName("SasiPerCope");
            this.Property(t => t.CmimiCope).HasColumnName("CmimiCope");
            this.Property(t => t.CopeNeStok).HasColumnName("CopeNeStok");
            this.Property(t => t.CopeNePorosi).HasColumnName("CopeNePorosi");
            this.Property(t => t.NivelPerseritjesPorosi).HasColumnName("NivelPerseritjesPorosi");
            this.Property(t => t.Nderprere).HasColumnName("Nderprere");

            // Relationships
            this.HasOptional(t => t.Furnizuesit)
                .WithMany(t => t.Produktets)
                .HasForeignKey(d => d.FurnizuesId);
            this.HasOptional(t => t.Kategorite)
                .WithMany(t => t.Produktets)
                .HasForeignKey(d => d.KategoriId);

        }
    }
}
