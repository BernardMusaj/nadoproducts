using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class TransportuesitMap : EntityTypeConfiguration<Transportuesit>
    {
        public TransportuesitMap()
        {
            // Primary Key
            this.HasKey(t => t.TransportuesId);

            // Properties
            this.Property(t => t.EmriKompanise)
                .IsRequired()
                .HasMaxLength(40);

            this.Property(t => t.Tel)
                .HasMaxLength(24);

            // Table & Column Mappings
            this.ToTable("Transportuesit");
            this.Property(t => t.TransportuesId).HasColumnName("TransportuesId");
            this.Property(t => t.EmriKompanise).HasColumnName("EmriKompanise");
            this.Property(t => t.Tel).HasColumnName("Tel");
        }
    }
}
