using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class PorosiDetajeMap : EntityTypeConfiguration<PorosiDetaje>
    {
        public PorosiDetajeMap()
        {
            // Primary Key
            this.HasKey(t => new { t.PorosiID, t.ProduktID });

            // Properties
            this.Property(t => t.PorosiID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ProduktID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("PorosiDetaje");
            this.Property(t => t.PorosiID).HasColumnName("PorosiID");
            this.Property(t => t.ProduktID).HasColumnName("ProduktID");
            this.Property(t => t.CmimiCope).HasColumnName("CmimiCope");
            this.Property(t => t.Sasia).HasColumnName("Sasia");
            this.Property(t => t.Zbritje).HasColumnName("Zbritje");

            // Relationships
            this.HasRequired(t => t.Porosite)
                .WithMany(t => t.PorosiDetajes)
                .HasForeignKey(d => d.PorosiID);
            this.HasRequired(t => t.Produktet)
                .WithMany(t => t.PorosiDetajes)
                .HasForeignKey(d => d.ProduktID);

        }
    }
}
