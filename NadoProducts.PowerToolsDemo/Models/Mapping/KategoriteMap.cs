using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class KategoriteMap : EntityTypeConfiguration<Kategorite>
    {
        public KategoriteMap()
        {
            // Primary Key
            this.HasKey(t => t.KategoriId);

            // Properties
            this.Property(t => t.KategoriEmer)
                .IsRequired()
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("Kategorite");
            this.Property(t => t.KategoriId).HasColumnName("KategoriId");
            this.Property(t => t.KategoriEmer).HasColumnName("KategoriEmer");
            this.Property(t => t.KategoriPershkrim).HasColumnName("KategoriPershkrim");
            this.Property(t => t.KategoriFoto).HasColumnName("KategoriFoto");
        }
    }
}
