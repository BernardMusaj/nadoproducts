using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class KlientDemografiteMap : EntityTypeConfiguration<KlientDemografite>
    {
        public KlientDemografiteMap()
        {
            // Primary Key
            this.HasKey(t => t.KlientTipId);

            // Properties
            this.Property(t => t.KlientTipId)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("KlientDemografite");
            this.Property(t => t.KlientTipId).HasColumnName("KlientTipId");
            this.Property(t => t.KlientPershkrim).HasColumnName("KlientPershkrim");

            // Relationships
            this.HasMany(t => t.Klientets)
                .WithMany(t => t.KlientDemografites)
                .Map(m =>
                    {
                        m.ToTable("KleintKlientDemo");
                        m.MapLeftKey("KlientTipId");
                        m.MapRightKey("KlientId");
                    });


        }
    }
}
