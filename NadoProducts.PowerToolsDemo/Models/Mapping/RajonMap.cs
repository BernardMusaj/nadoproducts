using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class RajonMap : EntityTypeConfiguration<Rajon>
    {
        public RajonMap()
        {
            // Primary Key
            this.HasKey(t => t.RajonId);

            // Properties
            this.Property(t => t.RajonId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.RajonPershkrim)
                .IsRequired()
                .IsFixedLength()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Rajon");
            this.Property(t => t.RajonId).HasColumnName("RajonId");
            this.Property(t => t.RajonPershkrim).HasColumnName("RajonPershkrim");
        }
    }
}
