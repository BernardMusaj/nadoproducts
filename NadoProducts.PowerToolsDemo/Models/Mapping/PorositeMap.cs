using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class PorositeMap : EntityTypeConfiguration<Porosite>
    {
        public PorositeMap()
        {
            // Primary Key
            this.HasKey(t => t.PorosiId);

            // Properties
            this.Property(t => t.KlientId)
                .IsFixedLength()
                .HasMaxLength(5);

            this.Property(t => t.DergeseEmere)
                .HasMaxLength(40);

            this.Property(t => t.DergeseAdres)
                .HasMaxLength(60);

            this.Property(t => t.DergeseQytet)
                .HasMaxLength(15);

            this.Property(t => t.DergeseRajon)
                .HasMaxLength(15);

            this.Property(t => t.DergeseKodiPostar)
                .HasMaxLength(10);

            this.Property(t => t.DergeseShtet)
                .HasMaxLength(15);

            // Table & Column Mappings
            this.ToTable("Porosite");
            this.Property(t => t.PorosiId).HasColumnName("PorosiId");
            this.Property(t => t.KlientId).HasColumnName("KlientId");
            this.Property(t => t.PuntorId).HasColumnName("PuntorId");
            this.Property(t => t.DataPorosi).HasColumnName("DataPorosi");
            this.Property(t => t.DataNevojshme).HasColumnName("DataNevojshme");
            this.Property(t => t.DataDerguar).HasColumnName("DataDerguar");
            this.Property(t => t.DerguarMe).HasColumnName("DerguarMe");
            this.Property(t => t.Ngarkesa).HasColumnName("Ngarkesa");
            this.Property(t => t.DergeseEmere).HasColumnName("DergeseEmere");
            this.Property(t => t.DergeseAdres).HasColumnName("DergeseAdres");
            this.Property(t => t.DergeseQytet).HasColumnName("DergeseQytet");
            this.Property(t => t.DergeseRajon).HasColumnName("DergeseRajon");
            this.Property(t => t.DergeseKodiPostar).HasColumnName("DergeseKodiPostar");
            this.Property(t => t.DergeseShtet).HasColumnName("DergeseShtet");

            // Relationships
            this.HasOptional(t => t.Klientet)
                .WithMany(t => t.Porosites)
                .HasForeignKey(d => d.KlientId);
            this.HasOptional(t => t.Puntoret)
                .WithMany(t => t.Porosites)
                .HasForeignKey(d => d.PuntorId);
            this.HasOptional(t => t.Transportuesit)
                .WithMany(t => t.Porosites)
                .HasForeignKey(d => d.DerguarMe);

        }
    }
}
