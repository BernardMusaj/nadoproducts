using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace NadoProducts.PowerToolsDemo.Models.Mapping
{
    public class PerdoruesitMap : EntityTypeConfiguration<Perdoruesit>
    {
        public PerdoruesitMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.PerdoruesEmri)
                .HasMaxLength(50);

            this.Property(t => t.Pass)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Perdoruesit");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.PerdoruesEmri).HasColumnName("PerdoruesEmri");
            this.Property(t => t.Pass).HasColumnName("Pass");
        }
    }
}
