using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class PorosiDetaje
    {
        public int PorosiID { get; set; }
        public int ProduktID { get; set; }
        public decimal CmimiCope { get; set; }
        public short Sasia { get; set; }
        public float Zbritje { get; set; }
        public virtual Porosite Porosite { get; set; }
        public virtual Produktet Produktet { get; set; }
    }
}
