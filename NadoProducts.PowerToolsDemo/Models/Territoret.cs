using System;
using System.Collections.Generic;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class Territoret
    {
        public Territoret()
        {
            this.Puntorets = new List<Puntoret>();
        }

        public string TerritorId { get; set; }
        public string TerritorPershkrim { get; set; }
        public int RajonId { get; set; }
        public virtual Rajon Rajon { get; set; }
        public virtual ICollection<Puntoret> Puntorets { get; set; }
    }
}
