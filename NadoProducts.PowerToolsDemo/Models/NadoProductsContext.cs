using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using NadoProducts.PowerToolsDemo.Models.Mapping;

namespace NadoProducts.PowerToolsDemo.Models
{
    public partial class NadoProductsContext : DbContext
    {
        static NadoProductsContext()
        {
            Database.SetInitializer<NadoProductsContext>(null);
        }

        public NadoProductsContext()
            : base("Name=NadoProductsContext")
        {
        }

        public DbSet<Furnizuesit> Furnizuesits { get; set; }
        public DbSet<Kategorite> Kategorites { get; set; }
        public DbSet<KlientDemografite> KlientDemografites { get; set; }
        public DbSet<Klientet> Klientets { get; set; }
        public DbSet<Perdoruesit> Perdoruesits { get; set; }
        public DbSet<PorosiDetaje> PorosiDetajes { get; set; }
        public DbSet<Porosite> Porosites { get; set; }
        public DbSet<Produktet> Produktets { get; set; }
        public DbSet<Puntoret> Puntorets { get; set; }
        public DbSet<Rajon> Rajons { get; set; }
        public DbSet<Territoret> Territorets { get; set; }
        public DbSet<Transportuesit> Transportuesits { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new FurnizuesitMap());
            modelBuilder.Configurations.Add(new KategoriteMap());
            modelBuilder.Configurations.Add(new KlientDemografiteMap());
            modelBuilder.Configurations.Add(new KlientetMap());
            modelBuilder.Configurations.Add(new PerdoruesitMap());
            modelBuilder.Configurations.Add(new PorosiDetajeMap());
            modelBuilder.Configurations.Add(new PorositeMap());
            modelBuilder.Configurations.Add(new ProduktetMap());
            modelBuilder.Configurations.Add(new PuntoretMap());
            modelBuilder.Configurations.Add(new RajonMap());
            modelBuilder.Configurations.Add(new TerritoretMap());
            modelBuilder.Configurations.Add(new TransportuesitMap());
        }
    }
}
