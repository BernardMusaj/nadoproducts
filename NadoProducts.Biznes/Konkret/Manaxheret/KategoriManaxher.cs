﻿using NadoProducts.Biznes.Abstrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;
using NadoProducts.DataAccess.Abstrakt;

namespace NadoProducts.Biznes.Konkret.Manaxheret
{
    public class KategoriManaxher : IKategoriServis
    {
        private IKategoryDAL _kategoriDal;

        public KategoriManaxher(IKategoryDAL kategoriDal)
        {
            _kategoriDal = kategoriDal;
        }

        public List<Kategori> GetAll()
        {
            return _kategoriDal.GetList();
        }
    }
}
