﻿using NadoProducts.Biznes.Abstrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;
using NadoProducts.DataAccess.Abstrakt;

namespace NadoProducts.Biznes.Konkret.Manaxheret
{
    public class PerdoruesManaxher : IPerdoruesServis
    {
        private IPerdoruesDAL _perdoruesDal;

        public PerdoruesManaxher(IPerdoruesDAL perdoruesDal)
        {
            _perdoruesDal = perdoruesDal;
        }

        public Perdorues GetByPerdoruesEmriAndPass(Perdorues perdorues)
        {
            return _perdoruesDal.Get(p => p.PerdoruesEmri == perdorues.PerdoruesEmri && p.Pass == perdorues.Pass);
        }
    }
}
