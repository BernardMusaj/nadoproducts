﻿using NadoProducts.Biznes.Abstrakt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete;
using NadoProducts.Entitete.Konkret;
using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.Biznes.RregullatValidimit.FluentValidation;

namespace NadoProducts.Biznes.Konkret.Manaxheret
{
    public class ProduktManaxher : IProduktServis
    {
        private IProduktDAL _produktDal;

        public ProduktManaxher(IProduktDAL produktDal)
        {
            _produktDal = produktDal;
        }

        private void KontrolloProduktEmer(Produkt produkt) //Metode qe kontrollon nese ky produkt(ne baze te ProduktEmri) ekziston ne DB
        {                                                   
            bool KaNdonjeProdukeEmerMeTeNjejtinEmer = _produktDal.GetList(p => p.ProduktEmer == produkt.ProduktEmer).Any();

            if (KaNdonjeProdukeEmerMeTeNjejtinEmer)
            {
                throw new Exception("Ndodhet nje produkt me te njejtin emer!");
            }
        }

        public void Add(Produkt produkt)
        {
            FluentValidatiorTool.Valido(new ProduktValidator(), produkt);
            KontrolloProduktEmer(produkt); //Perdorimi i metodes "KontrolloProduktEmri"

            _produktDal.Add(produkt);
        }

        public void Delete(Produkt produkt)
        {
            _produktDal.Delete(produkt);
        }

        public List<Produkt> GetAll(ProduktFilter produktFilter)
        {
            int? kategoriId = produktFilter.KategoriId;

            if (kategoriId != null)
            {
                return _produktDal.GetList(
                                           filter: produkt => produkt.KategoriId == kategoriId,
                                           orderBy: o => o.OrderBy(produkt => produkt.Id),
                                           page: produktFilter.Page,
                                           pageSize: produktFilter.PageSize
                                           );
            }else
            {
                return _produktDal.GetList(
                                           orderBy: o => o.OrderBy(produkt => produkt.Id),
                                           page: produktFilter.Page,
                                           pageSize: produktFilter.PageSize
                                           );
            }
        }

        public Produkt GetById(int id)
        {
            return _produktDal.Get(p => p.Id == id);
        }

        public List<Produkt> GetByKategori(int kategoriId)
        {
            return _produktDal.GetList(p => p.KategoriId == kategoriId);
        }

        public List<Produkt> GetByPerdoruesEmri(string produktEmri)
        {
            return _produktDal.GetList(p => p.ProduktEmer.Contains(produktEmri));
        }

        public int GetProduktetCountByKategori(int? kategoriId)
        {
            return _produktDal.GetProduktetCountByKategori(kategoriId);
        }

        public void Update(Produkt produkt)
        {
            FluentValidatiorTool.Valido(new ProduktValidator(), produkt);

            _produktDal.Update(produkt);
        }
    }
}
