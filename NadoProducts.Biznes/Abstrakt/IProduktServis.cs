﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NadoProducts.Entitete.Konkret;
using NadoProducts.Entitete;

namespace NadoProducts.Biznes.Abstrakt
{
    public interface IProduktServis
    {
        List<Produkt> GetAll(ProduktFilter produktFilter);

        Produkt GetById(int id);    //Merr produktin nga Id

        List<Produkt> GetByKategori(int kategoriId);    //Merr produktin nga Kategoria (Id e Kategori-se)

        List<Produkt> GetByPerdoruesEmri(string produktEmri); 

        void Add(Produkt produkt);

        void Update(Produkt produkt);

        void Delete(Produkt produkt);

        int GetProduktetCountByKategori(int? kategoriId);
    }
}
