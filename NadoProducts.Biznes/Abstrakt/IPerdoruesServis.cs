﻿using NadoProducts.Entitete.Konkret;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NadoProducts.Biznes.Abstrakt
{
    public interface IPerdoruesServis
    {
        Perdorues GetByPerdoruesEmriAndPass(Perdorues perdorues);
    }
}
