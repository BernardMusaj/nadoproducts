﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.Biznes.RregullatValidimit.FluentValidation
{
    public class ProduktValidator:AbstractValidator<Produkt>
    {
        public ProduktValidator()
        {
            RuleFor(t => t.ProduktEmer).NotEmpty();
            RuleFor(t => t.KategoriId).NotEmpty();
            RuleFor(t => t.SasiPerCope).NotEmpty().When(t=>t.KategoriId == 1);
            RuleFor(t => t.CmimiCope).GreaterThan(0);
            RuleFor(t => t.ProduktEmer).Must(RregulliJuaj);
        }

        private bool RregulliJuaj(string arg)
        {
            return true;
        }
    }
}
