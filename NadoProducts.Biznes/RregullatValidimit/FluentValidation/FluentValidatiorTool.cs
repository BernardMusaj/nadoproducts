﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NadoProducts.Biznes.RregullatValidimit.FluentValidation
{
    public class FluentValidatiorTool
    {
        public static void Valido(IValidator validator, object entitet)  //Per te validuar nje objekt, na duhet objketi the validator tool
        {
            var rezultati = validator.Validate(entitet);
            if (rezultati.Errors.Count > 0)
            {
                throw new ValidationException(rezultati.Errors);
            }
        }
    }
}
