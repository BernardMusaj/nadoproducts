﻿using System.Collections.Generic;
using System.Web.Mvc;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.MvcWebUI.Models
{
    public class ProduktAddViewModel
    {
        public List<SelectListItem> Kategorite { get; set; }
        public Produkt Produkt { get; set; }
    }
}