﻿using System.Collections.Generic;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.MvcWebUI.Models
{
    public class KategoriListViewModel
    {
        public int? KategoriIdAktuale { get; set; }
        public List<Kategori> Kategorite { get; set; }
    }
}