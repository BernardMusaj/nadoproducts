﻿using System.Collections.Generic;
using NadoProducts.Entitete.Konkret;

namespace NadoProducts.MvcWebUI.Models
{
    public class ProduktListViewModel
    {
        public PagingInfo PagingInfo { get;  set; }
        public List<Produkt> Produktet { get;  set; }
    }
}