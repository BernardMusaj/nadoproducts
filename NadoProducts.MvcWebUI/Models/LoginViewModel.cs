﻿using NadoProducts.Entitete.Konkret;

namespace NadoProducts.MvcWebUI.Models
{
    public class LoginViewModel
    {
        public Perdorues Perdorues { get; set; }
        public bool MeMbajMend { get; set; }
    }
}