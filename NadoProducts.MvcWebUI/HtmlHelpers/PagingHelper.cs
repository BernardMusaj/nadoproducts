﻿using NadoProducts.MvcWebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NadoProducts.MvcWebUI.HtmlHelpers
{
    public static class PagingHelper            //Krijimi i logjikes pagination
    {
        public static MvcHtmlString Pager(this HtmlHelper helper, PagingInfo pagingInfo)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append("<ul class='pagination pull-right'>"); //klase bootstrap-i

            if (pagingInfo.CurrentPage != 1)
            {
                int previous = pagingInfo.CurrentPage - 1;
                stringBuilder.AppendFormat("<li><a href=\"{0}{1}\">&laquo;</a></li>", //Krijohet butoni "previous" nqs nk jemi tek el 1
                    pagingInfo.BaseUrl, previous);
            }

            for (int i = 1; i <= pagingInfo.TotalPageCount; i++)
            {
                stringBuilder.Append(string.Format("<li class='{2}'><a  href='{1}{0}'>{0}</a></li>", i, //per cdo faqe krijon nje list item (link)
                    pagingInfo.BaseUrl, pagingInfo.CurrentPage == i ? "active" : string.Empty)); //nqs psh i=3, ath list item-it me text "3" i japim klasen "active"
            }

            if (pagingInfo.CurrentPage < pagingInfo.TotalPageCount)
            {
                int next = pagingInfo.CurrentPage + 1;
                stringBuilder.AppendFormat("<li><a href=\"{0}{1}\">&raquo;</a></li>",   //Krijohet butoni "next"
                    pagingInfo.BaseUrl, next);
            }

            stringBuilder.Append("</ul>");
            return MvcHtmlString.Create(stringBuilder.ToString());
        }
    }
}