﻿using NadoProducts.Biznes.Abstrakt;
using NadoProducts.MvcWebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NadoProducts.MvcWebUI.Controllers
{
    public class KategoriController : Controller
    {
        private IKategoriServis _kategoriServis;

        public KategoriController(IKategoriServis kategoriServis)
        {
            _kategoriServis = kategoriServis;
        }

        public PartialViewResult List(int? kategoriId) //Krijojme Listen e Kategorive si nje Partial View
        {
            return PartialView(new KategoriListViewModel
            {
                Kategorite = _kategoriServis.GetAll(),
                KategoriIdAktuale = kategoriId //Ktu vendoset vlera (e cila vjen nga el qe eshe klikuar
                                               //dhe kalon si parameter kategoriId) e KategoriIdAktuale
            });
        }
    }
}