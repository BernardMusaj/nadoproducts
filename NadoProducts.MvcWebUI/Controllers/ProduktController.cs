﻿using NadoProducts.Biznes.Abstrakt;
using NadoProducts.Biznes.Konkret;
using NadoProducts.Biznes.Konkret.Manaxheret;
using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.DataAccess.Konkret.EntityFramework;
using NadoProducts.Entitete;
using NadoProducts.Entitete.Konkret;
using NadoProducts.MvcWebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NadoProducts.MvcWebUI.Controllers
{
    [Authorize]
    public class ProduktController : Controller
    {
        private IProduktServis _produktServis;
        private IKategoriServis _kategoriServis;

        public ProduktController(IProduktServis produktServis, IKategoriServis kategoriServis) //Nqs nk pedorim Ninject do te kemi error 
                            //tek konstruktori me parametra
        {
            _produktServis = produktServis;
            _kategoriServis = kategoriServis;
        }

        public int PageSize = 10;

        [AllowAnonymous]
        public ActionResult Index(int? kategoriId, int page=1)
        {
            int produktCount = _produktServis.GetProduktetCountByKategori(kategoriId);
            var produktet = _produktServis.GetAll(new ProduktFilter
            {
                KategoriId = kategoriId,
                Page = page,
                PageSize = PageSize
            });
            return View(new ProduktListViewModel
            {
                Produktet = produktet,
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    CurrentKategori = kategoriId,
                    TotalPageCount = (int)Math.Ceiling((decimal)produktCount/PageSize), //Formule per pagination
                    BaseUrl=String.Format("/Produkt/Index?kategoriId={0}&page=",kategoriId)
                }
            });
        }
        [HttpGet]
        public ActionResult Shto()
        {
            return View(new ProduktAddViewModel
            {
                Produkt = new Produkt { CmimiCope = 50 }, //Nuk eshte e nevojshme qe te krijojm nje instance te "Produkt" por nqs duam te japim vlera default.
                Kategorite = _kategoriServis.GetAll().Select(item=>new SelectListItem()
                { Text = item.KategoriEmer, Value = item.Id.ToString()}).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Shto(Produkt produkt)
        {
            TempData.Add("Mesazhi", "Produkti u shtua me sukses!"); //TempData hoston info vetm per nje request
            _produktServis.Add(produkt);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Ndrysho(int Id)
        {
            return View(new ProduktAddViewModel
            {
                Produkt = _produktServis.GetById(Id),
                Kategorite = _kategoriServis.GetAll().Select(item => new SelectListItem()
                { Text = item.KategoriEmer, Value = item.Id.ToString() }).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Ndrysho(Produkt produkt)
        {
            _produktServis.Update(produkt);
            TempData.Add("Mesazhi", "Produkti u ndryshua me sukses!");
            return RedirectToAction("Index");
        }

        public ActionResult Fshij(int id)
        {
            var produktiPerTuFshire =_produktServis.GetById(id);
            _produktServis.Delete(produktiPerTuFshire);
            TempData.Add("Mesazhi", "Produkti u fshi!");
            return RedirectToAction("Index");
        }
    }
}