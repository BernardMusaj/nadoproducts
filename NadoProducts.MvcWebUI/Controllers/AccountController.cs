﻿using NadoProducts.Biznes.Abstrakt;
using NadoProducts.Entitete.Konkret;
using NadoProducts.MvcWebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NadoProducts.MvcWebUI.Controllers
{
    public class AccountController : Controller
    {
        private IPerdoruesServis _perdoruesServis;

        public AccountController(IPerdoruesServis perdoruesServis)
        {
            _perdoruesServis = perdoruesServis;
        }

        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Perdorues perdorues, bool MeMbajMend=false)
        {
            Perdorues perdoruesPerKontroll = _perdoruesServis.GetByPerdoruesEmriAndPass(perdorues);
            if (perdoruesPerKontroll == null)
            {
                TempData.Add("Mesazhi", "Gabim ne username ose password");
                return View();
            }

            FormsAuthentication.SetAuthCookie(perdorues.PerdoruesEmri, MeMbajMend);
            return RedirectToAction("Index","Produkt");
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Produkt");
        }
    }
}