﻿using FluentValidation;
using NadoProducts.Biznes.RregullatValidimit.FluentValidation;
using NadoProducts.Entitete.Konkret;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NadoProducts.MvcWebUI.DependencyResolvers
{
    public class FluentValidatorFactory : ValidatorFactoryBase
    {
        private IKernel _kernel;

        public FluentValidatorFactory()
        {
            _kernel = new StandardKernel();
            _kernel.Bind<IValidator<Produkt>>().To<ProduktValidator>().InSingletonScope(); //Nqs entiteti "Prdoukt" duhet te validohet ath perdorim ProduktValidator
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return (validatorType == null) ? null : (IValidator)_kernel.TryGet(validatorType);
        }
    }
}