﻿using NadoProducts.Biznes.Abstrakt;
using NadoProducts.Biznes.Konkret.Manaxheret;
using NadoProducts.DataAccess.Abstrakt;
using NadoProducts.DataAccess.Konkret.EntityFramework;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NadoProducts.MvcWebUI.DependencyResolvers
{
    public class NinjectControllerFactory:DefaultControllerFactory
    {
        private IKernel _kernel;

        public NinjectControllerFactory()
        {
            _kernel = new StandardKernel();

            _kernel.Bind<IProduktServis>().To<ProduktManaxher>().InSingletonScope();    //Kur psh kontrolleri perdor IProduktServis ath lidhim ate me ProduktManaxher
            _kernel.Bind<IProduktDAL>().To<EfProduktDal>().InSingletonScope();

            _kernel.Bind<IKategoriServis>().To<KategoriManaxher>().InSingletonScope();
            _kernel.Bind<IKategoryDAL>().To<EfKategoriDal>().InSingletonScope();

            _kernel.Bind<IPerdoruesServis>().To<PerdoruesManaxher>().InSingletonScope();
            _kernel.Bind<IPerdoruesDAL>().To<EfPerdoruesDal>().InSingletonScope();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null? null : (IController) _kernel.Get(controllerType); //Kontrollojm nese eshte null, nese po e leme null, 
                                                                                             //zgjidh kontrollerin por duke marr vlerat nga "_kernel.Get(controllerType)"
        }
    }
}